#!/usr/bin/env bash
########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 25/11/2020
#version: 1.0.0
########################################################################


count="1"
dev="/dev/"
name_vg="pams_cake"


main() {
local messge1="Like sdb,sdc.. etc: "
local messge2="another disk or mount if not enter"
clear
printf "%s\n Enter name of disk or mount path by letter. \n $messge1"
read sdb
printf "\n $messge2 \n $messge1"
read sdc
printf "\n $messge2 \n $messge1"
read sdd

_sleep

u_mount
rm_partition
create_part_8e
create_pv_vg
create_lv

}


#un mount the all disks 

u_mount() {


local mnt_map=/mnt/sd

num_mounts=($(cat /proc/mounts | grep /dev/sd |tail -n +2 | wc -l))

if [[ $num_mounts == 0 ]]; then
        printf "%s\n not mounted \n "
	_sleep
else

while [ $count -le $num_mounts ]; do

umount $mnt_map$sdb$count 2> /dev/null
umount $mnt_map$sdc$count 2> /dev/null
umount $mnt_map$sdd$count 2> /dev/null
((count++))
done
printf "%s \n umounted \n"
        _sleep
fi

}

############################################################################


#remove all disk in partitions.
#remove all LVM like lv,vg,pv.
#wipe fs

rm_partition() {


local num_part=($(fdisk -l | grep ^/dev/sd | tail -n +3 | wc -l))

if [[ $num_part -eq 0 ]]; then

printf "%s\n @@@ no partitions @@@\n\n "
	_sleep
else

lvremove -f /dev/$name_vg*
vgremove -f pams_cake
pvremove -f /dev/sd{b..d}*

printf "%s\n REMOVED all LVM. \n"
	_sleep
while [ $count -le $num_part ]; do

parted $dev$sdb rm $count 2> /dev/null
printf "%s \n removed partition. \n"
parted $dev$sdc rm $count 2> /dev/null
printf "%s \n removed partition.. \n"
parted $dev$sdd rm $count 2> /dev/null
printf "%s \n removed partition... \n"


wipefs -af /dev/sdb1 2> /dev/null
wipefs -af /dev/sdc1 2> /dev/null
wipefs -af /dev/sdd1 2> /dev/null

printf "%s \n removed partition....$count \n"
	_sleep
((count++))

done

fi

}

###########################################################################

# Create partitions for LVM 8e type.
create_part_8e() {

local hdd="$dev$sdb $dev$sdc $dev$sdd"
for i in $hdd;do
parted $i --script -- mkpart primary 0% 100%
printf 't\n8e\nw' | fdisk $i
done


printf "%s\n created partitions with LVM  \n "
        _sleep
}







###########################################################################


#Ceate PV  and  VG.

create_pv_vg() {
printf "%s\n Name of disk for create LVM.\n"

pvs -a | grep /dev | awk '{print $1}' >> device
cat device
read -p "Entered disk for LVM. Like sdb1,sdc1.. etc:" sdb1
read -p "another one.  Like sdb1,sdc1.. etc:" sdc1
	
rm -rf device
	_sleep

l_pvs=($(pvs -a | grep /dev |wc -l))
if [[ $l_pvs -ge 3 ]]; then

local _hdd="$dev$sdb1 $dev$sdc1"
for i in $_hdd;do
pvcreate $i;done

vgcreate $name_vg $dev$sdb1 $dev$sdc1
printf "%s\n Created PV and VG. \n"
	_sleep
else
printf "%s \n Not found disk for create PV or VG. \n"
	_sleep
fi
}
###########################################################################

# for extend VG.

for_extend() {

lvremove -f /dev/$name_vg/*
sleep 4
printf "%s\n REMOVE lv for extend. \n"
	_sleep

local vg_n=($(vgs -a | grep -o $name_vg))

if [[ "$vg_n" == "$name_vg" ]]; then
printf "%s\nEntered disk name for extend. \n"
read -p "Like sdb1,sdc1.. etc: " ex

pvcreat $dev$ex 2> /dev/null
vgextend $name_vg $dev$ex

printf "%s\n Created PV and VG. \n"
        _sleep


create_lv

else
printf "%s\n The name of VG not found\n"
	_sleep
fi

}

##########################################################################


create_lv() {
local num="1"

local n_lv=($(vgs -a | grep -o $name_vg))

if [[ "$n_lv" == "$name_vg" ]]; then

while [[ $num -le 2 ]]; do

lvcreate -L 500M $name_vg

((num++))

done
printf "%s\n Created lv for LVM. \n"
	_sleep
chose
else
printf "%s \n Not found the name of LV \n"
	_sleep
fi
chose

}


_sleep() {
sleep 2.5
clear
}

chose() {

PS3='Please enter your choice: '
options=("For create LVM chose 1" "For extend LVM chose 2" "Quit 3")
select opt in "${options[@]}"
do
    case $opt in
        "For create LVM chose 1")
            echo "you chose choice 1";main
            ;;
        "For extend LVM chose 2")
            echo "you chose choice 2";for_extend
            ;;
        "Quit 3")
            exit 1
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
}



chose $@
