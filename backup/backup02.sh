#!/usr/bin/env bash

########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 11/09/2020
#version: 1.0.0
########################################################################


###########################################################################
local map="$@" 2> /dev/null

back_up_mbr() {
if [[ -z $map ]] ; then
        message_exist
        exit 1
else
        printf "%s\n back up mbr:\n"
        dd if=$map of=backup.mbr bs=2047 count=1
        echo ""
        message_not_exist
fi
}
###########################################################################
local map="$@" 2> /dev/null


home_partition() {
if [[ -z $map ]] ; then
        message_exist
        exit 1
else
        printf "%s\n Back up home partition :\n "
    dd if=$map of=backup_home.img
        message_not_exist
fi
}
###########################################################################
local map="$@" 2> /dev/null

backup_zip_home() {
        if [[ -z $map ]] ; then
        message_exist
        exit 1
else

        printf "%s\n Back up and zip home partition: \n"

        dd if=$map | gzip -c > backup_zip_home.img.gz
    message_not_exist
fi
}
###########################################################################
local map="$@" 2> /dev/null

backup_zip_root() {
    if [[ -z $map ]] ; then
        message_exist
        exit 1
else

        printf "%s\n Back up and zip root:\n "
        dd if=$map | gzip -c > backup_zip_root.img.gz
        message_not_exist
fi
}
###########################################################################
local map="$@" 2> /dev/null

backup_zip_disk() {
    if [[ -z $map ]] ; then
        message_exist
        exit 1
else

        printf "%s\n Back up and zip whole disk:\n "
        dd if=$map | gzip -c > backup_zip_disk.img.gz
        message_not_exist
fi
}
###########################################################################
clean_awap() {
printf ""
swapoff -z && swapon -z
printf "%s \n Check swap \n "
free -h | grep Swap
sleep 3
clear
exit 1
}
###########################################################################
message_exist(){
        _time=2.5
        l="##############################"
        printf "\n$l\n # %s\n$l\n" "This file is exist"
        sleep $_time
        clear
}
###########################################################################
message_not_exist(){
        _time=2.5
        l="##############################"
        printf "\n$l\n # %s\n$l\n" "The file was created and completed"
        sleep $_time
        clear
}
menu(){
local map="$@"

echo -ne "
Back up Menu

1) Back up mbr
2) Back up home partition
3) Back up and zip home partition
4) Back up and zip root partition
5) Back up and zip whole disk
6) Clean awap
0) Exit
 Choose an option: "

        read a
        case $a in
                1) back_up_mbr ; menu ;;
                2) home_partition ; menu ;;
                3) backup_zip_home ; menu ;;
                4) backup_zip_root ; menu ;;
                5) backup_zip_disk ; menu ;;
                6) clean_awap ; menu ;;
                0) exit 0 ;;
                *) printf "Wrong option."$clear; WrongCommand; menu ; sleep 3 ;;
       esac
}

# Call the menu function

menu "$@"
